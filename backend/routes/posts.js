const express = require("express");
const multer = require("multer");
const Post = require("../models/post");

const router = express.Router();
const checkAuth = require("../middleware/check-auth");

const MIME_TYPE_MAP = {
  "image/png": "png",
  "image/jpeg": "jpg",
  "image/jpg": "jpg"
};
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const isValid = MIME_TYPE_MAP[file.mimetype];
    let error = new Error("Invalid mime type");
    if (isValid) {
      error = null;
    }
    cb(error, "backend/images");
  },
  filename: (req, file, cb) => {
    const name = file.originalname
      .toLocaleLowerCase()
      .split(" ")
      .join("-");
    const ext = MIME_TYPE_MAP[file.mimetype];
    cb(null, name + "-" + Date.now() + "." + ext);
  }
});

//save new post in db
router.post(
  "",
  checkAuth,
  multer({ storage: storage }).single("image"),
  (req, res, next) => {
    const url = req.protocol + "://" + req.get("host");
    const post = new Post({
      title: req.body.title,
      content: req.body.content,
      imagePath: url + "/images/" + req.file.filename
    });
    post.save().then(createdPost => {
      res.status(201).json({
        message: "Post added successfully",
        post: {
          ...createdPost,
          id: createdPost._id
        }
      });
    });
  }
);

//load all posts
router.get("", (req, res, next) => {
  const pageSize = +req.query.pagesize;
  const currentPage = +req.query.page;
  const postQuery = Post.find();
  let fatchedPosts;
  if (pageSize && currentPage) {
    postQuery
      //don't load (pageSize * (currentPage + 1)) posts
      .skip(pageSize * (currentPage - 1))
      // get only (pageSize) posts
      .limit(pageSize);
  }
  postQuery
    .then(documents => {
      fatchedPosts = documents;
      return Post.countDocuments();
    })
    .then(count => {
      res.status(200).json({
        message: "Posts fatched succesfully",
        data: fatchedPosts,
        maxPosts: count
      });
    });
});

//load one post by id from path
router.get("/:id", (req, res, next) => {
  Post.findById(req.params.id).then(post => {
    if (post) {
      res.status(200).json({
        message: "Post found",
        data: post
      });
    } else {
      res.status(404).json({
        message: "Post not found"
      });
    }
  });
});

//update post
router.put(
  "/:id",
  checkAuth,
  multer({ storage: storage }).single("image"),
  (req, res, next) => {
    let imagePath = req.body.imagePath;

    if (req.file) {
      const url = req.protocol + "://" + req.get("host");
      imagePath = url + "/images/" + req.file.filename;
    }
    const post = new Post({
      _id: req.body.id,
      title: req.body.title,
      content: req.body.content,
      imagePath: imagePath
    });
    Post.updateOne({ _id: req.params.id }, post).then(result => {
      res.status(200).json({
        message: "Post updated",
        data: post
      });
    });
  }
);

//delete post
router.delete(
  "/:id",
  checkAuth,
  (req, res, next) => {
    Post.deleteOne({ _id: req.params.id }).then(result => {
      res.status(200).json({ message: "Post deleted" });
    });
  }
);

module.exports = router;
