import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { AuthService } from "../auth.service";
import { Auth } from "../auth.model";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  isLoading = false;
  constructor(public authService: AuthService) {}

  ngOnInit() {}
  onLogin(form: NgForm) {
    if (form.invalid) {
      return;
    }
    this.isLoading=true;
    const auth: Auth = {
      email: form.value.email,
      password: form.value.password
    };

    this.authService.login(auth);
  }
}
